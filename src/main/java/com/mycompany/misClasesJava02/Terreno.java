package com.mycompany.misClasesJava02;

public class Terreno {

    protected int numLote;
    protected float ancho;
    protected float largo;

    public Terreno(int numLote, float ancho, float largo) {
        this.numLote = numLote;
        this.ancho = ancho;
        this.largo = largo;
    }

    public Terreno() {
        this.numLote = 0;
        this.ancho = 0.0f;
        this.largo = 0.0f;
    }

    public Terreno(Terreno v) {
        this.numLote = v.numLote;
        this.ancho = v.ancho;
        this.largo = v.largo;
    }

    public int getNumLote() {
        return numLote;
    }

    public void setNumLote(int numLote) {
        this.numLote = numLote;
    }

    public float getAncho() {
        return ancho;
    }

    public void setAncho(float ancho) {
        this.ancho = ancho;
    }

    public float getLargo() {
        return largo;
    }

    public void setLargo(float largo) {
        this.largo = largo;
    }

    public float calcularPerimetro() {
        return 2 * (this.ancho + this.largo);
    }

    public float calcularArea() {
        return this.ancho * this.largo;
    }
}
